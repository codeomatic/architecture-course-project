# Existing system
Based on project description and assumptions here is the representation of existing system

## Physical view with main elements
![Physical schematic with main elements](./schematics/existing-bot-physical-view.svg)

We assume the battery is big enough to power the robot for at least 6 hours. All units are monitored and controlled by computational unit.

WiFi is used to transfer data and provide user interface to the robot.

Computational unit has Linux based OS with ROS installed and enough computational power to process sensor data and manage other robot units.

## Module structure
![Existing module structure](./schematics/existing-module-structure.svg)
