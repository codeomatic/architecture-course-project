# Architecture Course Project

Course project for Architecture class.

# Report

## Outline
* [Problem Description](#problem-description)
* [Context Modifier](#context-modifier)
* [Assumptions based on the problem description](#assumptions-based-on-the-problem-description)
* [Business goals](#business-goals)
* [Architectural Drivers](#architectural-drivers)
* [Engineering objective](#engineering-objective)
* [Quality attributes mapping](#quality-attributes-mapping)
* [Design Process](#design-process)
* [Existing system](#existing-system)
* [Design iterations over quality attributes](#design-iterations-over-quality-attributes)
* [Integration with a separate project](#integration-with-a-separate-project)
* [Alternative approaches](#alternative-approaches)

## Problem Description
You should design a service for robotic navigation. 

The purpose of this service is to enable a robotic system to reliably explore its environment. 

The navigation stack is based on ROS. The navigation stack uses two costmaps to store information about robot obstacles. One costmap is used for global planning, meaning creating long-term plans over the entire environment, and the other is used for local planning and obstacle avoidance. The navigation stack has parameters that determine the maximum range sensor reading that will result in an obstacle being put into the costmap. Moreover, it contains parameters that inflate the set of obstacles in the environment. When these parameters have higher values, the robot navigates more safely (as the robot assumes that it is close to obstacles).

[outline](#outline)

## Context Modifier 
The robot application that needs to navigate in mountain terrain. Here, the performance of the robot is of an extreme importance, while the safety is on medium level as there are no humans in the surroundings. The robot should be able to navigate with high velocity, low precision and energy-efficient manners.

[outline](#outline)

## Assumptions based on the problem description
We need to add a navigation stack based on existing robot that can move
through mountain terrain. It is designed to operate without refueling or recharging
for useful amount of time. In mountainous conditions the operation time should not be less than 6 hours.
Cruising speed should be not less than 20 km/h in order to map useful area of terrain.
Based on that the robot should be big enough to accommodate large battery.
We can assume the robot can move through mountain terrain
if somebody gives it precise directions. For example, a human can control it 
using a game controller. Our system will control the robot in place of a human. As the robot moves through an area, we store a map of terrain and obstacles.

The best way to navigate unknown mountain and probably forested terrain is to use 
walking robot similar to Boston Dynamics 4 legged robot. It can claim over small
obstacles that would be difficult for whiled robots. We also assume that the system
uses WiFi protocol for user interface and data download.

One possible deployment scenario:

Team of forest fire fighters is deployed to remote forested area. For the team safety
it is important to know a real walkable escape path in case fire starts moving 
toward them faster than expected. When team is deployed the robot can start mapping
terrain around the team and scout for possible escape routes.

[outline](#outline)

## Business goals
* Autonomously provide teams deployed in mountain terrain with up-to-date obstacle map and other points of interest. 
* Reconnaissance support in areas dangerous for humans

[outline](#outline)

## Architectural Drivers
* Integrate into an existing robot control interface.
* Low or no wireless connection (the robot operates in mountain terrain).
* Integrate into an existing onboard sensor system.
* Later components of the system will be extended to operate in a different context.
* Can't iterate on actual hardware (crashing it often is expensive).
* Multiple behavioral patterns depending on situations
* There will be other components of the robot developed by other teams. If the robot malfunctions, there needs to be a way to trace what went wrong. Integrate into the robot's telemetry system.

[outline](#outline)

## Engineering objective
* Provide autonomous terrain mapping
* Support different mapping modes for different conditions
* Given coordinates and search pattern, take a robot through an unknown area
* At the end of operation provide a map of the area
* Allow robot to use in different scenarios 

[outline](#outline)

## Quality attributes mapping
Mapping of quality attributes to business goals, architectural drivers, and engineering objectives.
This answers the question why those attributes are important.
* Functional correctness
    - Maps can be incomplete but must be correct to support the business goal
* Testability
    - Hard to iterate on actual hardware (crashing is expensive)
* Analysability
    - Trace and reproduce failures (other teams work on different components)
    - Check for malfunctioning components before each trip because loosing hardware is expensive
* Modularity
    - Integrate with other projects or components
    - Integrate into an existing robot control interface.
* Modifiability
    - Components of the system will be extended to operate in a different context 
* Fault Tolerance
    - Tolerate some components failure in order to save the hardware or partially complete the mission
* Configureability
    - Multiple behavioral patterns depending on the situation
* Maintainability
    - Deal with different sets of hardware  

[outline](#outline)

## Design Process
* List [business goals](#business-goals).
* List [actions](#assumptions-based-on-the-problem-description) that the system needs to perform.
* Based on business goals, determine relevant [quality attributes](#quality-attributes-mapping).
* Null iteration design: Based on vocab and actions, create [initial design](./quality_attributes_iterations.md#initial-iteration).
    - Dynamic view
    - Static view
    - Integration into other components
* Iterate design over quality attributes.

[outline](#outline)

## [Existing system](./existing_system_description.md)
[Description of existing system](./existing_system_description.md) based on initial requirements and assumptions.

[outline](#outline)

## [Design iterations over quality attributes](./quality_attributes_iterations.md)
Based on the existing system 'null' iteration was designed. [Each new iteration](./quality_attributes_iterations.md)
supports required quality attribute.

[outline](#outline)

## [Integration with a separate project](./integration_with_sbc.md)
The goal of this section is to describe integration with Single Board Computer (SBC)
which takes camera input and produces object frames as an output. The SBC also outputs
status metadata and has control API. The [SBC integration](./integration_with_sbc.md) 
doesn't pose any problems since the project assumes different types of sensors can be added to the system.

[outline](#outline)

## Alternative approaches
* Further separate configuration control into a separate module
* Implement sensor adapters on the main board side
* Move navigation into another single board computer (problem loading different configs)
* Sensor data duplication

[Top](#report)