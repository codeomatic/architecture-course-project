# Quality attributes iterations
The main goal of the 'null' iteration is to provide required functionality to the [existing system](./existing_system_description.md).

## Initial iteration
Functional correctness. The goal of the initial iteration is to add autonomous navigation.
We preserve most of the existing modules and functionality. We remove manual control
and replace it with navigation unit.

![initial iteration navigation module](./schematics/initial-iteration-navigation-module.svg)


## Testability (iteration 1)
Since deploying and testing each change on actual hardware is impractical, emulation setup is introduced.
ROS can be deployed on hardware that doesn't have access to actual hardware components. We can use
prerecorded or simulated data to test how our system behaves in different scenarios.

![integration testing setup](./schematics/integration-testing-setup.svg)


## Analysability (iteration 2)
Instead of subscribing to individual API monitor modules we subscribe to
general log topic. We can replay sensor logs directly into navigation module 
and figure out what went wrong or analyze navigation behavior.

![analysability](./schematics/analysability.png)


## Modularity (iteration 3)
Integrating into existing robot control and processing various sets of inputs is complicated.
In order to manage that complexity we'll split the navigation module into several components.
General log topic aggregates inputs from all the sensors so we need a filter to pick only
relevant data to detect obstacles and environment change. We push the data into a buffer.
We don't push the data directly into decision making component because it might be still
processing previous chunk of data. Using buffer we let decision making component to process
previous chunks and adjust if the data rate changes.

![navigation module](./schematics/navigation.png)


## Configureability (iteration 4)

Here we'll focus on the navigation module. Navigation needs to adopt to changing terrain 
and sensor condition. Some sensors are better for one set of conditions than another.
We can have multiple behavior patterns depending on time of day or type of terrain.

![self adaptation](./schematics/self-adaptation.png)

## Modifiability
Using self-adoption pattern, our navigation module now can take into account 
new hardware. We can add new configuration that monitors for input from new
sensors and makes a decision based on that input.

## Fault tolerance
Using same self-adoption pattern we can monitor for failed hardware and adjust our
operational mode. For example if one leg failed it is still possible to the base
using 3 remaining legs.

## Maintainability
Adjustments to navigation behavior can be made through configuration. This is
important for accommodating different types of hardware and operating in remote
areas. Let's say the operator had to replace a sensor. It has slightly different
parameters. Adjusting code base is not an option but tunning the behavior using
configuration is possible. 
