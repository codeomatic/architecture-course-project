# Project Initial Draft

* [Problem Description](#problem-description)
* [Context Modifier](#context-modifier)
* [Business Goals](#business-goals)
* [Architectural Drivers](#architectural-drivers)
* [Supporting Quality Requirement](#supporting-quality-requirement)
* [Design Process](#design-process)

## Problem Description
You should design a service for robotic navigation. 

The purpose of this service is to enable a robotic system to reliably explore its environment. 

The navigation stack is based on ROS. The navigation stack uses two costmaps to store information about robot obstacles. One costmap is used for global planning, meaning creating long-term plans over the entire environment, and the other is used for local planning and obstacle avoidance. The navigation stack has parameters that determine the maximum range sensor reading that will result in an obstacle being put into the costmap. Moreover, it contains parameters that inflate the set of obstacles in the environment. When these parameters have higher values, the robot navigates more safely (as the robot assumes that it is close to obstacles).

## Context Modifier 
The robot application that needs to navigate in mountain terrain. Here, the performance of the robot is of an extreme importance, while the safety is on medium level as there are no humans in the surroundings. The robot should be able to navigate with high velocity, low precision and energy-efficient manners.

## Business Goals
There is a robotic system. We can assume it can move through mountain terrain
if somebody gives it precise directions. For example, a human can control it 
using a game controller. Our system will control the robot in place of a human. As the robot moves through an area, we store a map of terrain and obstacles.

* Given coordinates and search pattern, take a robot through an unknown area.
* At the end of operation provide a map of the area.
* reclaim robot after use (configure speed)

## Architectural Drivers
* Integrate into an existing robot control interface.
* Low or no wireless connection (the robot operates in mountain terrain).
* Integrate into an existing onboard sensor system.
* Later components of the system will be extended to operate in a different context.
* Can't iterate on actual hardware (crashing it often is expensive).
* Processing speed constrain
* There will be other components of the robot developed by other teams. If the robot malfunctions, there needs to be a way to trace what went wrong. Integrate into the robot's telemetry system.

Add quality attributes from architecture using ROS
Add safety 

ISO 25010 map to business goals
| business goal | engineering objective | quality attributes

## Supporting Quality Requirement
The design is done in iterative steps. Each step will address one quality attribute at a time.
Where do we get execution time, etc.

## Design Process
* List business goals.
* Create initial vocabulary with definitions.
* List actions that the system needs to perform.
* Keep updating vocab as design evolves.
* Based on business goals, determine relevant quality attributes.
* Null iteration design: Based on vocab and actions, create initial design.
    - Types of data
    - Dynamic view (UML)
    - Static view (UML)
    - Integration into other components (UML)
* Iterate design over quality attributes.

[Top](#project-initial-draft)