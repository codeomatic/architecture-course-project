# Integration with SBC
The goal of this section is to describe integration with Single Board Computer (SBC)
which takes camera input and produces object frames as an output. The SBC also outputs
status metadata and has control API.

![SBC](./schematics/sbc.png)

In order to accommodate different sensors and environments our navigation system uses a self-adaptation pattern. If a sensor or another component fails, the navigation module can pick a strategy that works with remaining hardware. Adding another type of sensor works well with a self-adaptation pattern because we just add another configuration that knows how to get and what to do with new type of data.

![self adaptation](./schematics/self-adaptation.png)

When the system is booting up, the configuration describing the new SBC is loaded. When the condition analyzer detects metadata from our SBC we load an appropriate filter and a decision maker. This config will skip the obstacle detector since it is already built into the SBC. If the board is turned off during operations, we’ll fall back on other sensors and the initial navigation configuration stack.

Besides the navigation module here is how the SBC integrates into the entire system:

![SBC integration](./schematics/integrated.png)

We need additional modules to adopt output to work with ROS. This can be implemented
on the SBC or on the main board. It's probably going to be easier to implement it
on the main board because it a general purpose computer. Besides adopting output
the connection is similar to any other sensor connection.

![component view](./schematics/physical-component-view.png)